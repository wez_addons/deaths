-----------------------------------------------
-- Globals to locals (speed up calls)
-----------------------------------------------
local _G = getfenv(0)
local GetZone = _G.GetRealZoneText
local GetLevel = _G.UnitLevel
local GetName = _G.UnitName
local Print, List = _G.WezUtils.GetPrinter('ffffff', '777777', 'ff0000'), _G.WezUtils.List
local strformat = string.format

-----------------------------------------------
-- Namespace (d)
-----------------------------------------------
local _, d = ...
local pid = d.constants.session.player.id
local DeathService = d.service.DeathService
local PlayerDetailsService = d.service.PlayerDetailsService
local Death = d.dto.Death

local function DebugChatFrames()
  for i = 1, NUM_CHAT_WINDOWS do
    _G["ChatFrame"..i.."EditBox"]:SetAltArrowKeyMode(false)
  end
end

-----------------------------------------------
-- Event functions / Events
-----------------------------------------------
d.Events = {}
local Events = d.Events

function Events:ADDON_LOADED(name)
  if (name == "Deaths") then
    local count = #(DeathService:SearchDeathsByPlayer(pid))
    Print(strformat("Welcome back %s! You have died %s times so far, stay safe.", GetName("player"), count))
    DebugChatFrames()
    PlayerDetailsService:InitPlayerDetails()
  end
end

function Events:TIME_PLAYED_MSG(arg1, arg2)
  print(arg1, arg2)
end

function Events:PLAYER_DEAD(...)
  local death = Death(GetLevel("player"), GetZone(), time())
  DeathService:SaveDeath(death)
end

function Events:PLAYER_LEVEL_UP(...)
  C_Timer.After(1, function()
    local currLvl = UnitLevel("player")
    local lastLvl = currLvl - 1
    DeathService:PrintPlayerDeathsByLevel(pid, lastLvl)
    Print("Stay safe on level", currLvl .. "!")
  end)
end

-----------------------------------------------
-- Event frame
-----------------------------------------------

-- Create own frame for event listener
local LoadEvents = CreateFrame("Frame")

-- Iterate all events from above and register them
for k, v in pairs (d.Events) do
  LoadEvents:RegisterEvent(k)
end

-- Generic handler that calls the handlers from above
LoadEvents:SetScript("OnEvent", function(self, event, ...) Events[event](event, ...) end )
