--#region Imports
local strlower, strformat = string.lower, string.format

local _, d = ...
local player = d.constants.session.player
local ToggleGUI = d.ui.ToggleGUI
local Death = d.dto.Death

local cString = _G.WezUtils.ColoredString
local Print = _G.WezUtils.GetPrinter("ffffff", "777777", "ff0000")

local DeathService = d.service.DeathService
local OptionsService = d.service.OptionsService

--#endregion

-----------------------------
-- Append cmds with function names corresponding to slashcmd names
-----------------------------
local cmds = {}

function cmds.print()
  DeathService:PrintPlayerDeaths(player.id)
end

---

function cmds.printByLevel(level)
  DeathService:PrintPlayerDeathsByLevel(player.id, level)
end

---

-- function cmds.ui()
--   ToggleGUI()
-- end
--
-- cmds.gui = cmds.ui

---

function cmds.options()
  local options = OptionsService:GetOptions()

  if (options) then
    Print("options = {")
    for k, v in pairs(options) do
      Print(strformat("    %s = %s", k, tostring(v)))
    end
    Print("}")
  end

end

---

function cmds.help()
  print(" ")
  Print("List of commands")
  Print("|cff00aacc/deaths gui/ui|r - Shows graphical menu")
  Print("|cff00aacc/deaths|r - Lists all deaths in chat")
  Print("|cff00aacc/deaths <number>|r - Lists deaths on given level (e.g. /deaths 50)")
  print(" ")
end

---

function cmds.save()
  local death = Death(
    UnitLevel("player"),
    GetRealZoneText(),
    time()
  )

  DeathService:SaveDeath(death)
end

---

function cmds.clear()
  DeathService:DeletePlayerData(player.id)
end

---------------------------------------------
-- Handler that calls the correct function --
---------------------------------------------

function HandleSlash(msg)
  local first = strlower(strsplit(" ", msg, 2))

  if (#first > 0) then
    if (tonumber(first)) then
      cmds.printByLevel(tonumber(first))
    elseif (cmds[first]) then
       cmds[first]()
    else
      cmds.help()
    end
  else
    cmds.print()
  end
end

-- local function HandleSlash(msg)
-- end

SLASH_deaths1 = "/deaths"
SlashCmdList["deaths"] = HandleSlash

-- for k, v in pairs(cmds) do
--   local slashcmd = string.join("", "/", k)
--   local slashvar = string.join("", "SLASH_", k, "1")
--   _G[slashvar] = slashcmd
--   SlashCmdList[k] = cmds[k]
-- end


