# Deaths

<img src="./deaths-logo.jpg" height="350" />

## Description

Once the addon is installed, it will collect data from your deaths for all
characters that the addon is enabled on.

The addon will print you some statistics when you either die or hit next level.

---

## Slash Commands

> **/deaths** - Lists all deaths of your character so far <br/> <br/> >
> **/deaths gui** - Opens the graphical user interface / configuration window of
> the addon <br/> <br/> > **/deaths _number_** - Lists deaths on given level
> (e.g. /deaths 50) <br/> <br/> > **TODO: /deaths _number1_ _number2_** - Lists
> deaths during levels between given levels (e.g. /deaths 5 10 lists all your
> deaths between levels 5 and 10)

---

## Limitations

### Old characters

There is no way to find out how many times your character has already died.
Deaths will only be collected from the moment the addon is enabled.

---
