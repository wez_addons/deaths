local WezUtils = _G.WezUtils
if (not WezUtils) then
  WezUtils = {}
  _G["WezUtils"] = WezUtils
end

local Methods = {}

local type = WezUtils.type
local List = WezUtils.List

if (not List) then
  List = {}
  WezUtils["List"] = List

  local reservedKeys = {
    map = true,
    filter = true,
    forEach = true,
  }

  --- Initialize typed list with List({...}, "string")
  --- or non-typed list with just List()
  ---@param oldTbl? table the table to initialize with
  ---@param t? string type of the list defaults to "any"
  ---@return "List<T>"
  function List:GiveNewArray(oldTbl, t)
    local arr = {}

    if (oldTbl and table.getn(oldTbl) > 0) then
      for i,v in ipairs(oldTbl) do
        local typeOfV = type(v)
        if (t and typeOfV ~= t) then
          return error("Attempting to insert item of type " .. typeOfV .. "  into list of type List<" .. t .. "> at index: " .. i)
        end
        table.insert(arr, v)
      end
    end


    setmetatable(arr, {
      __index = function(tbl, key)
        -- if trying to index .type of the list, return type as a string eg. "List<T>"
        if (key == 'type') then return ('List<' .. (t or 'any') .. '>')
        -- If  trying to index one of the defined methods, return a function that takes given parameters
        elseif Methods[key] then
          return function(...)
            return Methods[key](tbl, t, ...)
          end
        end
      end,

      __newindex = function(tbl, key, val)
        error("Use methods to manipulate the list")
      end
    })

    return arr
  end

  setmetatable(List, { __call = List.GiveNewArray })
end

--- Iterates through each item in list tbl and
--- executes function f for each item returns a new list
--- based on function f's return value
---@param tbl "List<T>"
---@param t "T"
---@param f function
---@return "List<T>"
function Methods.map(tbl, t, f)
  local obj = List:GiveNewArray()
  for k, v in pairs(tbl) do
    table.insert(obj, f(v, k))
  end
  return obj
end

--- Iterates through each item in list tbl and
--- executs function f. Based on f's return value we either
--- keep or dont keep the item in newly created list
---@param tbl "List<T>"
---@param t "T"
---@param f "function -> boolean"
---@return "List<T>"
function Methods.filter(tbl, t, f)
  local obj = List:GiveNewArray()
  for k, v in pairs(tbl) do
    if f(v, k) then table.insert(obj, v) end
  end
  return obj
end

--- Iterates through each item in list tbl and
--- executes function f. If a value is returned, stop iteration and
--- return newly returned vlaue
---@param tbl "List<T>"
---@param t "T"
---@param f "function -> boolean"
---@return "any"
function Methods.forEach(tbl, t, f)
  for k, v in pairs(tbl) do
    local res = f(v, k)
    if (res) then return res end
  end
end

--- Append an item to a list, if typed list check that item is of the correct type
---@param tbl "List<T>"
---@param t "T"
---@param v "T"
---@return "void"
function Methods.push(tbl, t, v)
  local typeOfV = type(v)
  if (t and typeOfV ~= t) then
    return error("Attempting to insert item of type " .. typeOfV .. "  into list of type List<" .. t .. ">")
  end

  table.insert(tbl, v)
end

--- Set item at specific index, if typed list check that item is of correct type
---@param tbl "List<T>"
---@param t "T"
---@param pos "integer"
---@param v "T"
---@return "void"
function Methods.insert(tbl, t, pos, v)
  local typeOfV = type(v)
  if (t and typeOfV ~= t) then
    return error("Attempting to insert item of type " .. typeOfV .. "  into list of type List<" .. t .. ">")
  end

  table.insert(tbl, pos, v)
end
