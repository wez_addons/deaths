local WezUtils = _G["WezUtils"]
if not WezUtils then
  WezUtils = WezUtils or {}
  _G["WezUtils"] = WezUtils
end

local addon_name = ...
local strformat, strjoin =  _G.string.format, _G.string.join

function WezUtils.type(value)
  local old = type(value)
  if (old == "number" and not string.find(tostring(value), "%.")) then return "integer" end
  if (old ~= "table") then return old end

  return value.type or old
end

function WezUtils.Class(name, ...)
  -- "cls" is the new class
  local cls, bases = {}, {...}
  -- copy base class contents into the new class
  for i, base in ipairs(bases) do
    for k, v in pairs(base) do
      cls[k] = v
    end
  end
  -- set the class's __index, and start filling an "instanceof" table that contains this class and all of its bases
  -- so you can do an "instance of" check using my_instance.instanceof[MyClass]
  cls.__index, cls.instanceof = cls, {[cls] = true}
  for i, base in ipairs(bases) do
    for c in pairs(base.instanceof) do
      cls.instanceof[c] = true
    end
    cls.instanceof[base] = true
  end

  -- the class's __call metamethod
  setmetatable(cls, {
    __call = function (c, ...)
      local instance = setmetatable({}, c)
      local constructor = instance.constructor
      if constructor then
        local args = {...}
        local types = constructor(instance, ...) or {}

        if (#types ~= #args) then
          return error(name .. ": wrong number of arguments: " .. #args .. " (should be " .. #types .. ")")
        end

        for i, v in ipairs(args) do
          local typeOfArg = WezUtils.type(args[i])
          if (types[i] ~= typeOfArg) then
            return error(name .. ": provided: " .. typeOfArg .. " but needed: " .. types[i])
          end
        end
      end

      return instance
    end,
    __index = function(c, k)
      if k == "type" then

        return name or "table"
      end
    end
  })
  -- return the new class table, that's ready to fill with methods
  return cls
end

function WezUtils.ColoredString(color, ...)
  return strformat("|cff%s%s|r", color, strjoin(" ", ...))
end

function WezUtils.GetPrinter(primary, secondary, highlight)
  return function(...)
--    local prefix = strformat(
--      "|cff%s[|r|cff%s%s|r|cff%s]|r",
--      highlight, secondary, addon_name, highlight)
    local prefix2 =
      WezUtils.ColoredString(highlight, "[") ..
      WezUtils.ColoredString(secondary, addon_name) ..
      WezUtils.ColoredString(highlight, "]")
    DEFAULT_CHAT_FRAME:AddMessage(strjoin(" ", prefix2, ...))
  end
end


---Usage: Provide type of the dto/type
---@param name string this is name for the type of dto
---@return "table:name" immutable table with initialized fields (not modifiable)
function WezUtils.CreateDto(name)
  local class = {}

  setmetatable(class, {
    __call = function(tbl, ...)
      local dto = {}
      local constructor = tbl.Constructor

      if (constructor) then
        constructor(dto, ...)
        local args = {...}
        local fields = tbl.fields or {}
        local fields_len = 0

        for _ in pairs (fields) do fields_len = fields_len + 1 end

        -- Check that the amount of arguments given to constructor
        -- matches the amount defined fields for the class
        if (fields_len ~= #args) then
          return error(name .. ": wrong number of arguments: " .. #args .. " (should be " .. fields_len .. ")")
        end

        -- Loop through actual fields of the created instance
        -- and compare defined type and the actual values given by constructor.
        -- Return error if invalid types detected
        for k, v in pairs(dto) do
          local tv = WezUtils.type(v)
          if (not fields[k]) then
            error("Undefined field: " .. k)
          end
          if (fields[k] ~= tv) then
            error("Wrong type provided for " .. k .. " (" .. fields[k] .. "): " .. tv)
          end
        end
      end

      --- if we index any property of instance,
      --- we first check whether it is a class property. If not, then we
      --- check whether it is data property
      --- this makes it impossible to store data with field name clashing with class methods
      --- a.k.a cant override methods as a data field
      local dtoMt = {
        __index = function(_, key)
          if (key == "type") then return name or "table" end
          if (tbl[key]) then return tbl[key] end
          if (dto[key]) then return dto[key] end
        end,
        __newindex = function(_, key, val) error(name .. ": Trying to insert into a non-existent field: " .. key) end,
        __unm = function(_)
          return dto
        end
      }
    return setmetatable({}, dtoMt)
    end,
    __index = function(_, key)
      if (key == "type") then return name or "table" end
    end
  })

  return class
end

function WezUtils.DeepCopy(orig)
  local orig_type = type(orig)
  local copy
  if orig_type == 'table' then
      copy = {}
      for orig_key, orig_value in next, orig, nil do
          copy[WezUtils.DeepCopy(orig_key)] = WezUtils.DeepCopy(orig_value)
      end
      setmetatable(copy, WezUtils.DeepCopy(getmetatable(orig)))
  else -- number, string, boolean, etc
      copy = orig
  end
  return copy
end