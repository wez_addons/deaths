local WezUtils = _G.WezUtils
if (not WezUtils) then
  WezUtils = {}
  _G["WezUtils"] = WezUtils
end

local WezDB = WezUtils.DB
if (not WezDB) then
  WezDB = {}
  WezUtils["DB"] = WezDB
end

local DBName -- db is the default name for saved variable maybe we could initialize it here to it
local DBFunctions = {}

---------------------------------------
-- Private utility functions (used by the public functions internally)
---------------------------------------
--#region Private
local function removeFirstFromPath(pathList)
  local newPathList = {}
  for i, v in ipairs(pathList) do table.insert(newPathList, i, v) end
  table.remove(newPathList, 1)
  return newPathList
end

local function SetPathValue(obj, pathList, value)
  if(table.getn(pathList) == 1) then
    if (pathList[1] == '#') then
      table.insert(obj, value)
    else
      obj[pathList[1]] = value
    end
    return true
  end

  if (not obj[pathList[1]]) then obj[pathList[1]] = {} end
  return SetPathValue(obj[pathList[1]], removeFirstFromPath(pathList), value)
end

local function GetPathValue(obj, pathList)
  if(table.getn(pathList) == 1) then
    return obj[pathList[1]]
  end

  if (not obj[pathList[1]]) then return end
  return GetPathValue(obj[pathList[1]], removeFirstFromPath(pathList))
end
--#endregion


function WezDB:GetDB(sv)
  if (type(sv) ~= "string") then
    error("wezdb should be initialized with variable name")
    return
  end

  DBName = sv
  _G[DBName] = _G[DBName] or {}

  local db = {}
  for funcName, func in pairs(DBFunctions) do db[funcName] = func end

  return db
end

-------------------------------------------
-- These are returned for the user to modify the data
-------------------------------------------

---Usage: Saves any value into path. If path does not exist, it will be created. use # to append to a list
---@param path string eg. 'my.path' if you wish to append a list use 'my.paths.#' otherwise the value will be rewritten
---@param value any
---@return boolean succesful true or false based whether the save was succesful or not
function DBFunctions:SaveInPath(path, value)
  if not _G[DBName] then error("Could not find global variable") return false end
  return SetPathValue(_G[DBName], {strsplit(".", path)}, value)
end

function DBFunctions:GetByPath(path)
  if (not _G[DBName]) then error("Could not get the global variable") return end
  local res = GetPathValue(_G[DBName], {strsplit(".", path)})
  if (type(res) == "table") then return WezUtils.DeepCopy(res) end

  return res
end

function DBFunctions:ClearPath(path)
  return DBFunctions:SaveInPath(path, nil)
end