--------------------------------------------------
-- Defines the shape of "classpath" or namespace of the addon
-- This helps in preventing nil errors when creating new services
--------------------------------------------------

local _, d = ...
d.constants = {}
d.dao = {}
d.dto = {}
d.service = {}
d.ui = {}
d.util = {}

