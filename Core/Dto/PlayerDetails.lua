local CreateDto = WezUtils.CreateDto
local _, d = ...

local PlayerDetails = CreateDto("PlayerDetails")

PlayerDetails.fields = {
  startLevel = "integer",
  startTime = "integer",
  startPlayed = "string",
}

---Creates a PlayerDetails object with given data
---@param startLevel integer
---@param startTime integer
---@param startPlayed string
function PlayerDetails:Constructor(startLevel, startTime, startPlayed)
  self.startLevel = startLevel
  self.startTime = startTime
  self.startPlayed = startPlayed
end


---Tries to convert object into PlayerDetails object - useful for converting database item into typed object
---@param obj table
---@return "PlayerDetails: PlayerDetails"
function PlayerDetails:From(obj)
  return PlayerDetails(obj.startLevel, obj.startTime, obj.startPlayed, obj.deaths)
end


d.dto.PlayerDetails = PlayerDetails
