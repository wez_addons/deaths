local _, d = ...
local CreateDto = _G.WezUtils.CreateDto
local type = _G.WezUtils.type
local maxLevel = d.constants.maxlevel

local Death = CreateDto("Death")

Death.fields = {
  level = "integer",
  zone = "string",
  date = "integer"
}

---@param1 level "integer" Level on which the death happened
---@param2 zone "string" Zone name
---@param3 date "integer" Unix time
---@return table
function Death:Constructor(level, zone, date)
  self.level = level
  self.zone = zone
  self.date = date
end


---Tries to convert any object to a Death
---@param obj any
---@return table
function Death.From(obj)
  return Death(obj.level, obj.zone, obj.date)
end

---Throws error if any business errors are found in the Death
function Death:Validate()
  if (self.level > maxLevel) then error("too high level") end
  if (self.date > time()) then error("the death is in future") end
end

d.dto.Death = Death

--- tests
