local _G = _G
local _, d = ...

local strformat = string.format

local PlayerName = UnitName("player")
local PlayerRealm = GetRealmName()

-- constants include any information that stays  same within the whole game session
-- Using these prevent the need from calling WoWAPI functions repeatedly within the addon

d.constants = {
  maxlevel= 70,
  session = {
    player = {
      id = strformat("%s-%s", PlayerName, PlayerRealm),
      name = PlayerName,
      realm = PlayerRealm,
    }
  }
}