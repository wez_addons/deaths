-- d imports
local _, d = ...
local PlayerDetails = d.dto.PlayerDetails
local PlayerDetailsDao = d.dao.PlayerDetailsDao
local pid = d.constants.session.player.id


-- Register service
local PlayerDetailsService = {}
d.service.PlayerDetailsService = PlayerDetailsService

function PlayerDetailsService:InitPlayerDetails()
  local details = PlayerDetailsService:SearchPlayerDetailsByPlayer(pid)
  if (details) then print("nothing") return end
  -- level, time, played
  RequestTimePlayed()
end

function PlayerDetailsService:SearchPlayerDetailsByPlayer(pid)
  return PlayerDetailsDao:SearchPlayerDetailsByPlayer(pid)
end

function PlayerDetailsService:SavePlayerDetails(details)
  PlayerDetailsDao:SavePlayerDetails(pid, details)
end