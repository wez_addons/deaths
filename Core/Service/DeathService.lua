local _G = getfenv(0)
local Print, type = _G.WezUtils.GetPrinter('ffffff', '777777', 'ff0000'), _G.WezUtils.type
local strformat = string.format


-- d imports
local _, d = ...
local Death = d.dto.Death
local DeathDao = d.dao.DeathDao
local pid = d.constants.session.player.id


-- Register service
local DeathService = {}
d.service.DeathService = DeathService

-----------------------------------
--     DeathService methods      --
-----------------------------------
--#region

---------------
-- Searches  --
---------------
--#region

---This can be called to get other than current character's deaths too
---@param pid string unique identifier of the player, eg. ("Wezd-Noggenfogger")
---@return "List<Death>"
function DeathService:SearchDeathsByPlayer(pid)
  return DeathDao:SearchDeathsByPlayer(pid)
end

---This can be called to get other than current character's deaths too
---@param pid string
---@param level integer
---@return "List<Death>"
function DeathService:SearchDeathsByPlayerAndLevel(pid, level)
  return DeathDao:SearchDeathsByPlayerAndLevel(pid, level)
end

--#endregion

------------
-- Prints --
------------
--#region

---Private function to print a single death
---@param death "Death" The death to print
local function PrintDeath(death)
  Print(strformat("[%s] %s - level %s", date(nil, death.date), death.zone, death.level))
end

---Prints all deaths of the provided player
---@param pid string Unique identifier of the player, eg. ("Wezd-Noggenfogger")
function DeathService:PrintPlayerDeaths(pid)
  local deaths = DeathService:SearchDeathsByPlayer(pid)
  Print(table.getn(deaths),"deaths in total:")

  deaths.forEach(PrintDeath)
end

---Prints all deaths of the provided player on the provided level
---@param pid string unique id of player "<Charname>-<Realmname>"
---@param level "integer" which level's deaths are we requesting
function DeathService:PrintPlayerDeathsByLevel(pid, level)
  local deaths = DeathService:SearchDeathsByPlayerAndLevel(pid, level)
  Print(table.getn(deaths), "deaths in total on level", level, ":")

  deaths.forEach(PrintDeath)
end

--#endregion

---------------------------------
--       Save & Delete         --
---------------------------------
--#region

---Deletes all player data from database
---@param pid "string" unique id of player "<Charname>-<Realmname>"
---@return boolean successful was the removal successful
function DeathService:DeletePlayerData(pid)
  return DeathDao:DeletePlayerData(pid)
end

---Validates given death and attempts to save the death
---@param death "Death" death to be saved
---@return boolean successful was the save successful
function DeathService:SaveDeath(death)
  if (type(death) ~= Death.type) then error("Trying to save incorrect type " .. type(death)) end
  return DeathDao:SaveDeath(pid, death)
end

--#endregion
--#endregion