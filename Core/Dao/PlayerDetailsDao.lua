local _, d = ...
local Death = d.dto.Death
local pid = d.constants.session.player.id

local db = WezUtils.DB:GetDB("db")

-- Register service
local PlayerDetailsDao = {}
d.dao.PlayerDetailsDao = PlayerDetailsDao

function PlayerDetailsDao:SearchPlayerDetailsByPlayer(pid)
  return db:GetByPath(pid)
end