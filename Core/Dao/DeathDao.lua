local _G = _G
local List, DB, type = _G.WezUtils.List, _G.WezUtils.DB, _G.WezUtils.type

local strformat = string.format

local _, d = ...
local Death = d.dto.Death


local database = DB:GetDB("db")

local DeathDao = {}
d.dao.DeathDao = DeathDao

----------------------------------------
-- DeathsService methods
----------------------------------------

function DeathDao:SearchDeathsByPlayer(pid)
  local path = strformat("%s.deaths", pid)
  local res = List(database:GetByPath(path)).map(Death.From)
  return List(res, Death.type)
end

function DeathDao:SearchDeathsByPlayerAndLevel(pid, level)
  local res = DeathDao:SearchDeathsByPlayer(pid).filter(function(v, k)
    return v.level == level
  end)

  return res
end

function DeathDao:SaveDeath(pid, death)
  local path = strformat("%s.deaths.#", pid)
  return database:SaveInPath(path, -death)
end

function DeathDao:DeletePlayerData(pid)
  return database:ClearPath(pid)
end
