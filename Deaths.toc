## Interface: 20502
## Author: Vesa Pilvinen
## Title: Deaths
## Notes: Maintains data about the deaths of your characters
## SavedVariables: db

# ## Libs
Libs\Libs.xml

# ## Addon
Deaths.xml
